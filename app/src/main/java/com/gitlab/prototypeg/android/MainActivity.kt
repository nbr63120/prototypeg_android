package com.gitlab.prototypeg.android

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Switch

import com.github.megatronking.netbare.NetBare


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
		val intent = NetBare.get().prepare()
		if (!NetBare.get().isActive) {
			if (intent == null) {
				if (!NetBare.get().isActive) {
					NetBare.get().start((application as PrototypeG).config)
				}
				startGF()
			} else {
				startActivityForResult(intent, 0)
			}
		}
        setContentView(R.layout.activity_main)
        //Crashlytics.getInstance().crash();
        if (NetBare.get().isActive) {
            (findViewById<View>(R.id.vpn_service) as Switch).isChecked = true
        }
        (findViewById<View>(R.id.vpn_service) as Switch).setOnCheckedChangeListener { view, isChecked ->
            val intent = NetBare.get().prepare()
            if (isChecked) {
                if (intent == null) {
                    if (!NetBare.get().isActive) {
                        NetBare.get().start((application as PrototypeG).config)
						startGF()
                    }
                } else {
                    startActivityForResult(intent, 0)
                }
            } else {
                NetBare.get().stop()
            }
        }

        findViewById<View>(R.id.start_gf).setOnClickListener({ view -> startGF() })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                if (!NetBare.get().isActive) {
                    NetBare.get().start((application as PrototypeG).config)
                }

                startGF()
            } else {
                (findViewById<View>(R.id.vpn_service) as Switch).isChecked = false
                val intent = NetBare.get().prepare()
                startActivityForResult(intent, 0)
            }
        }
    }

    private fun startGF() {
        val launchIntent = packageManager.getLaunchIntentForPackage(
                PreferenceManager.getDefaultSharedPreferences(this)
                        .getString(getString(R.string.package_name), "")!!
        )
        if (launchIntent != null) {
            startActivity(launchIntent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.preference) {
            startActivity(Intent(this, SettingsActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

	override fun onResume() {
		super.onResume()
		if (NetBare.get().isActive) {
			(findViewById<View>(R.id.vpn_service) as Switch).isChecked = true
		}
	}
}
