package com.gitlab.prototypeg.android

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.google.gson.GsonBuilder
import java.io.File
import java.io.FileWriter

import java.util.ArrayList

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        val bundle = Bundle()
        val settingsFragment = SettingsFragment()
        val packageNames = ArrayList<String>()
        packageNames.add(getString(R.string.target_cn_uc))
        packageNames.add(getString(R.string.target_cn_bili))
        packageNames.add(getString(R.string.target_en))
        packageNames.add(getString(R.string.target_jp))
        packageNames.add(getString(R.string.target_tw))
        packageNames.add(getString(R.string.target_kr))
        val p2 = ArrayList<CharSequence>()
        for (s in packageNames) {
            try {
                packageManager.getPackageInfo(s, 0)
                p2.add(s)
            } catch (e: PackageManager.NameNotFoundException) {
            }

        }
        val entries = p2.toTypedArray()
        bundle.putCharSequenceArray("package_names", entries)
        settingsFragment.arguments = bundle
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings, settingsFragment)
                .commit()

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowCustomEnabled(true)
        }
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            val bundle = arguments
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
            val listPreference = findPreference<ListPreference>(getString(R.string.package_name))
            listPreference!!.entries = bundle!!.getCharSequenceArray("package_names")
            listPreference.entryValues = bundle.getCharSequenceArray("package_names")
        }

		override fun onPreferenceTreeClick(preference: Preference?): Boolean {
			if (preference!!.key == getString(R.string.export_data)) {
				val data = (context!!.applicationContext as PrototypeG).session.index
				if (data !== null) {
					val gson = GsonBuilder().setPrettyPrinting().create()
					val dir = context!!.getExternalFilesDir(null)!!.absolutePath + "/index.json"
					val fileWriter = FileWriter(dir, false)
					fileWriter.write(gson.toJson(data.data))
					fileWriter.flush()
					fileWriter.close()
					val intent = Intent()
					intent.action = Intent.ACTION_VIEW
					intent.setDataAndType(
							FileProvider.getUriForFile(context!!, context!!.packageName + ".fileprovider", File(dir)),
							"application/json"
					)
					intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
					startActivity(intent)
				}
			}

			return super.onPreferenceTreeClick(preference)
		}
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}