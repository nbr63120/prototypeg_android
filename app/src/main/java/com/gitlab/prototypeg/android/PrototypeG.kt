package com.gitlab.prototypeg.android

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.SharedPreferences
import android.os.Build

import androidx.preference.PreferenceManager

import com.github.megatronking.netbare.NetBare
import com.github.megatronking.netbare.NetBareConfig
import com.github.megatronking.netbare.http.HttpInjectInterceptor
import com.github.megatronking.netbare.http.HttpInterceptorFactory
import com.github.megatronking.netbare.ssl.JKS
import com.gitlab.prototypeg.Session
import org.threeten.bp.Instant
import java.util.Arrays
import java.util.concurrent.atomic.AtomicInteger

class PrototypeG : Application() {
    val session = Session()

    var jks: JKS? = null
        private set

    private var configBuilder: NetBareConfig.Builder? = null

    val config: NetBareConfig
        get() = configBuilder!!
                .addAllowedApplication(PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.package_name), "none")!!)
                .build()

	private val atomicInteger = AtomicInteger(2)

	public var lastResponseTime: Instant = Instant.now().plusSeconds(60 * 5);

    override fun onCreate() {
        super.onCreate()

        val preferences = PreferenceManager.getDefaultSharedPreferences(this)

        object : Thread() {
            override fun run() {
                Session.init()
            }
        }.start()

        jks = JKS(
                this, this.getString(R.string.app_name),
                this.getString(R.string.app_name).toCharArray(),
                this.getString(R.string.app_name),
                this.getString(R.string.app_name),
                this.getString(R.string.app_name),
                this.getString(R.string.app_name),
                this.getString(R.string.app_name)
		)

		/*
        if (!JKS.isInstalled(this, getString(R.string.app_name))) {
            try {
                JKS.install(this, this.getString(R.string.app_name), this.getString(R.string.app_name))
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        */

        configBuilder = NetBareConfig.defaultHttpConfig(jks!!, listOf(
				HttpInjectInterceptor.createFactory(
						PacketInjector(session, this)
				)
		))
                .newBuilder()

        registerNotificationChannels()

        NetBare.get().attachApplication(this, false)

        session.eventManager.registerListener(EventListener(this))
		session.networkManager.responseHandlerManager.registerHandler(ResponseListener(this))
    }

    private fun registerNotificationChannels() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			var notificationChannels : List<NotificationChannel> = listOf(
					NotificationChannel(
							VPN_SERVICE,
							getString(R.string.channel_vpn_service),
							NotificationManager.IMPORTANCE_DEFAULT
					),
					NotificationChannel(
							BUILD,
							getString(R.string.channel_build),
							NotificationManager.IMPORTANCE_DEFAULT
					),
					NotificationChannel(
							NOTICE,
							getString(R.string.channel_notice),
							NotificationManager.IMPORTANCE_DEFAULT
					),
					NotificationChannel(
							GET,
							getString(R.string.channel_get),
							NotificationManager.IMPORTANCE_DEFAULT
					),
					NotificationChannel(
							BATTLE_FINISH,
							getString(R.string.channel_battle_finish),
							NotificationManager.IMPORTANCE_DEFAULT
					)
			)
			notificationChannels = notificationChannels.filter { notificationManager.getNotificationChannel(it.id) === null }
			notificationManager.createNotificationChannels(notificationChannels)
		}
    }

	fun newNotificationId(): Int {
		return atomicInteger.getAndIncrement()
	}

	companion object {
		const val VPN_SERVICE = "vpn_service"
		const val BUILD = "build"
		const val NOTICE = "notice"
		const val GET = "get"
		const val BATTLE_FINISH = "battle_finish"
	}
}
